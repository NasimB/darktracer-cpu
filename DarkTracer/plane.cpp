#include "plane.h"
#include <SDL_stdinc.h>
#include <algorithm>

using namespace std;

Plane::Plane():
	normal(0, 1, 0)
{
}


Plane::Plane(const Vector3 position, Vector3 normal)
	: normal(normal.normalize())
{
	this->position = position;
}


Plane::Plane(const Vector3 position, Vector3 normal, const Vector3 color)
	: normal(normal.normalize())
{
	this->position = position;
	this->color = color;
}


Plane::Plane(const Vector3 position, Vector3 normal, const Vector3 color, const float radius)
	: normal(normal.normalize()),
	radius(radius)
{
	this->position = position;
	this->color = color;
}


Plane::~Plane()
{
}


bool Plane::Intersects(const Ray& ray, float & near)
{
	// assuming vectors are all normalized
	float denom = this->normal.dot(-ray.direction);
	if (denom > 1e-6) 
	{
		Vector3 p0l0 = ray.origin - position;
		float t = p0l0.dot(this->normal) / denom;
		if (t > 0.0f && t < near)
		{
			Vector3 intersection = ray.origin + ray.direction * t;
			if (radius > 0 && (intersection - position).length() > radius)
				return false;
			
			near = t;
			return true;
		}
	}
	return false;
}

void Plane::GetNormalAndColor(const Vector3& hitpoint, Vector3& normal, Vector3& color)
{
	normal = this->normal;

	float checkerboard_scale = 0.5f;

	Vector3 localPos = hitpoint - position;
	float intensity_x = sinf(localPos.x * M_PI * checkerboard_scale) > 0.0f ? 1.0f : 0.0f;
	float intensity_y = sinf(localPos.z * M_PI * checkerboard_scale) > 0.0f ? 0.0f : 1.0f;

	float intensity = intensity_x == intensity_y ? 0.1f : 1.0f;
	color = this->color * intensity;
}
