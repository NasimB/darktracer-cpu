#pragma once

#include "vector3.h"
#include "ray.h"

enum Material {
	Standard_Material = 0,
	Chrome_Material,
	Emissive_Material
};

class Object
{
public:
	Object();
	virtual ~Object();

	virtual bool Intersects(const Ray& ray, float &near) = 0;
	virtual void GetNormalAndColor(const Vector3& hitpoint, Vector3& normal, Vector3& color) = 0;

	Vector3 position;
	Vector3 color;
	Material material;
};
