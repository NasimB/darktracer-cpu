#pragma once

#include "object.h"

class Plane : public Object
{
public:
	Plane();
	Plane(Vector3 position, Vector3 normal);
	Plane(Vector3 position, Vector3 normal, Vector3 color);
	Plane(Vector3 position, Vector3 normal, Vector3 color, float radius);
	~Plane();

	// Returns true if it intersects with the ray and is the closest to its origin, false otherwise.
	bool Intersects(const Ray& ray, float &near) override;

	void GetNormalAndColor(const Vector3& hitpoint, Vector3& normal, Vector3& color) override;

	Vector3 normal;

	float radius = 0.0f;
};
