#include "camera.h"

#include <random>

#define M_PI 3.14159265359

Camera::Camera() :
	position(0, 2, 60)
{
}


Camera::~Camera()
{
}

int Camera::Rand(const int min, const int max)  // [min,max) .. ie, max is exclusive
{
	const int range = max - min;
	return (rand() % range) + min;
}

std::default_random_engine cgenerator;
std::uniform_real_distribution<float> cdistribution(0, 1);

Ray Camera::ComputePrimaryRay(const unsigned short width, const unsigned short height, const unsigned short x, const unsigned short y) const
{
	const float invWidth = 1 / float(width);
	const float invHeight = 1 / float(height);
	const float fov = 30;
	const float aspectratio = width / float(height);
	const float angle = tan(M_PI * 0.5f * fov / 180.0f);

	//const float xx = (2 * ((x + 0.5) * invWidth) - 1) * angle * aspectratio;
	//const float yy = (1 - 2 * ((y + 0.5) * invHeight)) * angle;

	float r1 = cdistribution(cgenerator)-0.5f;
	float r2 = cdistribution(cgenerator)-0.5f;

	const float xx = (2 * ((x + r1) * invWidth) - 1) * angle * aspectratio;
	const float yy = (1 - 2 * ((y + r2) * invHeight)) * angle;

	Ray ray(position, Vector3(xx, yy, -1));
	ray.direction.normalize();
	return ray;
}
