#include "sphere.h"

Sphere::Sphere() :
	radius(1.0f)
{
}


Sphere::Sphere(const Vector3 position, const float radius)
{
	this->position = position;
	this->radius = radius;
}


Sphere::Sphere(const Vector3 position, const float radius, const Vector3 color)
{
	this->position = position;
	this->radius = radius;
	this->color = color;
}


Sphere::~Sphere()
{
}


bool Sphere::SolveQuadratic(const float &a, const float &b, const float &c, float &x0, float &x1) const
{
	float discr = b * b - 4 * a * c;

	if (discr < 0)
		return false;

	if (discr == 0)
	{
		x0 = x1 = -0.5f * b / a;
	}
	else 
	{
		float q = (b > 0) ? -0.5f * (b + sqrt(discr)) : -0.5f * (b - sqrt(discr));
		x0 = q / a;
		x1 = c / q;
	}

	if (x0 > x1)
	{
		std::swap(x0, x1);
	}
	return true;
}


bool Sphere::Intersects(const Ray& ray, float &near)
{
	float t0, t1; // solutions for t if the ray intersects 
#if 0 
				  // geometric solution
	Vec3f L = center - orig;
	float tca = L.dotProduct(dir);
	// if (tca < 0) return false;
	float d2 = L.dotProduct(L) - tca * tca;
	if (d2 > radius2) return false;
	float thc = sqrt(radius2 - d2);
	t0 = tca - thc;
	t1 = tca + thc;
	if (t0 > t1) std::swap(t0, t1);
#else 
				  // analytic solution
	Vector3 L = ray.origin - position;
	float a = ray.direction.dot(ray.direction);
	float b = 2 * ray.direction.dot(L);
	float c = L.dot(L) - (radius * radius);

	if (!SolveQuadratic(a, b, c, t0, t1)) 
		return false;
#endif 

	if (t0 < 0)
	{
		t0 = t1; // if t0 is negative, let's use t1 instead 
		if (t0 < 0) 
			return false; // both t0 and t1 are negative 
	}

	if (t0 > near)
		return false;

	near = t0;
	return true;
}


void Sphere::GetNormalAndColor(const Vector3& hitpoint, Vector3& normal, Vector3& color)
{
	normal = (hitpoint - position).normalize();
	color = this->color;
}
