#include <Windows.h>

#include "SDL.h"
#pragma comment(lib, "SDL2.lib")

#define M_PI 3.14159265359

#define SPEED 20.0f

#include "raytracer.h"

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window *window = SDL_CreateWindow("DarkTracer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Raytracer::width, Raytracer::height, SDL_RENDERER_ACCELERATED);

	Raytracer* raytracer = new Raytracer(window);

	uint32_t last_tick_time = 0;
	float delta = 0;

	bool complete = false;
	while (!complete)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					raytracer->GetCamera()->position.x -= delta * SPEED;
					raytracer->ClearScreen();
					break;
				case SDLK_RIGHT:
					raytracer->GetCamera()->position.x += delta * SPEED;
					raytracer->ClearScreen();
					break;
				case SDLK_UP:
					raytracer->GetCamera()->position.z -= delta * SPEED;
					raytracer->ClearScreen();
					break;
				case SDLK_DOWN:
					raytracer->GetCamera()->position.z += delta * SPEED;
					raytracer->ClearScreen();
					break;
				case SDLK_ESCAPE:
					complete = true;
					break;
				}
				break;

			case SDL_KEYUP:
				break;

			case SDL_QUIT:
				complete = true;
				break;

			default:
				break;
			}
		}

		uint32_t tick_time = SDL_GetTicks();
		delta = (tick_time - last_tick_time) * 0.001f;
		last_tick_time = tick_time;

		raytracer->Render(delta);
	}

	delete raytracer;

	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
