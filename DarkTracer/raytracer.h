#pragma once

#include <vector>
#include "SDL.h"

#include "vector3.h"
#include "ray.h"
#include "camera.h"
#include "object.h"

class Raytracer
{
public:
	explicit Raytracer(SDL_Window* window);
	~Raytracer();

	Camera* GetCamera() { return &camera; }

	void ClearScreen();
	void Render(float deltaTime);

	
	Vector3 Trace(const Ray &ray, const std::vector<Object*> &objects, const int &depth) const;
	void DrawScreen(const int minHeight, const int maxHeight) const;

	static const unsigned short width = 960;
	static const unsigned short height = 540;

	static const unsigned int MAX_DEPTH = 4;

private:

	static bool RussianRoulette(const int &depth, float& probability);

	static Vector3 DirectIllumination(const Vector3& P, const Vector3& N, const Vector3& lightCentre, float lightRadius, const Vector3& lightColour, float cutoff);

	SDL_Window* m_window = nullptr;
	SDL_Renderer* m_renderer = nullptr;
	SDL_Texture* m_framebuffer = nullptr;
	SDL_Surface* m_surface = nullptr;

	Vector3* m_image;

	Camera camera;

	int frame = 0;

	// Scene
	Vector3 m_lightDirection;
	Vector3 m_lightColor;
	std::vector<Object*> m_objects;

	const Vector3 backgroundColor = Vector3(0.3f, 0.3f, 0.4f);
};
