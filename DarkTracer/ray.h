#pragma once

#include "vector3.h"

class Ray
{
public:
	Ray(Vector3 origin, Vector3 direction);
	~Ray();

	Vector3 origin;
	Vector3 direction;
};
