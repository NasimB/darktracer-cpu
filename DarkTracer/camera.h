#pragma once

#include "vector3.h"
#include "ray.h"

class Camera
{
public:
	Camera();
	~Camera();

	static int Rand(int min, int max);

	Ray ComputePrimaryRay(const unsigned short width, const unsigned short height, const unsigned short x, const unsigned short y) const;

	Vector3 position;
	Vector3 direction;
};
