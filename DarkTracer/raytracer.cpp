#include "raytracer.h"

#include <future>
#include <string>
#include <random>
#include <algorithm>

#include "sphere.h"
#include "plane.h"

using namespace std;

Raytracer::Raytracer(SDL_Window* window)
{
	m_window = window;

	m_surface = SDL_GetWindowSurface(m_window);

	m_image = new Vector3[width * height];

	// Middle sphere
	m_objects.emplace_back(new Sphere(
		Vector3(0, 0, -10),
		4.0f,
		Vector3(0.9f, 0.9f, 0.9f)
	));
	m_objects[m_objects.size() - 1]->material = Chrome_Material;

	// Up sphere
	m_objects.emplace_back(new Sphere(
		Vector3(0, 8, 0),
		2.0f,
		Vector3(0, 1, 0)
	));

	// Left sphere
	m_objects.emplace_back(new Sphere(
		Vector3(-8, 1, 6),
		4.0f,
		Vector3(1, 0, 0)
	));

	// Far Left sphere
	m_objects.emplace_back(new Sphere(
		Vector3(-15, 8, 0),
		4.0f,
		Vector3(1, 1, 1)
	));


	// Right sphere
	m_objects.emplace_back(new Sphere(
		Vector3(8, 1, 6),
		4.0f,
		Vector3(0, 0, 1)
	));

	// Far Right sphere
	m_objects.emplace_back(new Sphere(
		Vector3(15, 8, 0),
		4.0f,
		Vector3(1, 1, 0)
	));

	// Floor plane
	m_objects.emplace_back(new Plane(
		Vector3(0, -4, 0),
		Vector3(0, 1, 0),
		Vector3(1, 1, 1),
		40.0f
	));


	m_lightDirection = Vector3(-1.0, -1.0, -1.0);
	m_lightDirection = m_lightDirection.normalize();

	m_lightColor = Vector3(2.0, 2.0, 2.0);

	float intensity = 50.0f;
	m_objects.emplace_back(new Sphere(
		Vector3(0, 17.0f, 0.0f),
		3.0f,
		Vector3(intensity, intensity, intensity)
	));
	m_objects[m_objects.size() - 1]->material = Emissive_Material;
}


Raytracer::~Raytracer()
{
	for (Object* obj : m_objects)
	{
		delete obj;
	}

	//delete[] m_image;
}


void Raytracer::ClearScreen()
{
	Vector3* pixel = m_image;
	for (unsigned short y = 0; y < height; ++y)
	{
		for (unsigned short x = 0; x < width; ++x, ++pixel)
		{
			(*pixel) = Vector3(0, 0, 0);
		}
	}
	frame = 0;
}


void Raytracer::Render(const float deltaTime)
{
	/*if (frame > 99)
	{
		Sleep(33);
		return;
	}*/

	// Use context to destroy futures automaticaly (to synchronise before screen update)
	{
		int window_height = static_cast<int>(height);

		const int linesPerJob = window_height / 64;
		vector<future<void>> handlers;
		handlers.reserve(16);

		for (int i = 0; i < window_height; i += linesPerJob)
		{
			handlers.emplace_back(async(launch::async, &Raytracer::DrawScreen, this, i, min(i + linesPerJob, window_height)));
		}
	}

	frame++;

	SDL_UpdateWindowSurface(m_window);


	SDL_SetWindowTitle(m_window, ("DarkTracer 3.2 - Frames: " + to_string(frame) + " - Frametime: " + to_string(deltaTime) + "ms").c_str());
}


void Raytracer::DrawScreen(const int minHeight, const int maxHeight) const
{
	Vector3* pixel = m_image + width * minHeight;

	float frame_number = static_cast<float>(frame + 1);

	for (unsigned short y = minHeight; y < maxHeight; ++y)
	{
		for (unsigned short x = 0; x < width; ++x, ++pixel)
		{
			const Ray ray = camera.ComputePrimaryRay(width, height, x, y);

			const Vector3 value = Trace(ray, m_objects, 0);

			Vector3 new_pixel;

			if (frame_number > 0.0f)
			{
				Vector3 average = (*pixel);
				average = average + ((value - average) / frame_number);
				new_pixel = average;
			}
			else
			{
				new_pixel = value;
			}

			(*pixel) = new_pixel;

			// Reinhard tone mapping
			//new_pixel = new_pixel / (new_pixel + Vector3(1.0f, 1.0f, 1.0f));

			// Exposure tone mapping
			new_pixel.x = 1.0f - expf(-new_pixel.x * 2.0f);
			new_pixel.y = 1.0f - expf(-new_pixel.y * 2.0f);
			new_pixel.z = 1.0f - expf(-new_pixel.z * 2.0f);

			// Gamma color correction
			float r = max(min(powf(new_pixel.x, 2.2f), 1.0f), 0.0f);
			float g = max(min(powf(new_pixel.y, 2.2f), 1.0f), 0.0f);
			float b = max(min(powf(new_pixel.z, 2.2f), 1.0f), 0.0f);

			const int i = width * y + x;
			Uint32* pixelData = static_cast<Uint32*>(m_surface->pixels) + i;
			(*pixelData) = SDL_MapRGB(m_surface->format, static_cast<Uint8>(r * 255), static_cast<Uint8>(g * 255), static_cast<Uint8>(b * 255));
		}
	}
}


default_random_engine generator;
uniform_real_distribution<float> distribution(0.0f, 1.0f);


bool Raytracer::RussianRoulette(const int &depth, float & probability)
{
	if (depth <= 1)
	{
		probability = 1.0f;
		return true;
	}

	if (depth > MAX_DEPTH)
	{
		return false;
	}

	const float chance = 1.0f / static_cast<float>(depth - 1); // 0.5f; // Percentage of chance to survive
	const float value = distribution(generator); // Random value between 0 and 1
	if (value > chance)
	{
		return false;
	}
	probability = chance;
	return true;
}


float randf()
{
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}


void fresnel(Vector3 &I, const Vector3 &N, const float &ior, float &kr)
{
	float cosi = I.dot(N);
	float etai = 1, etat = ior;
	if (cosi > 0) { swap(etai, etat); }
	// Compute sini using Snell's law
	float sint = etai / etat * sqrtf(max(0.f, 1 - cosi * cosi));
	// Total internal reflection
	if (sint >= 1) {
		kr = 1;
	}
	else {
		float cost = sqrtf(max(0.f, 1 - sint * sint));
		cosi = fabsf(cosi);
		float Rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost));
		float Rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost));
		kr = (Rs * Rs + Rp * Rp) / 2;
	}
	// As a consequence of the conservation of energy, transmittance is given by:
	// kt = 1 - kr;
}


Vector3 uniformSampleHemisphere(const float &r1, const float &r2)
{
	// cos(theta) = r1 = y
	// cos^2(theta) + sin^2(theta) = 1 -> sin(theta) = srtf(1 - cos^2(theta))
	float sinTheta = sqrtf(1 - r1 * r1);
	float phi = 2 * M_PI * r2;
	float x = sinTheta * cosf(phi);
	float z = sinTheta * sinf(phi);
	return Vector3(x, r1, z);
}


void createCoordinateSystem(const Vector3 &N, Vector3 &Nt, Vector3 &Nb)
{
	if (fabs(N.x) > fabs(N.y))
		Nt = Vector3(N.z, 0, -N.x) / sqrtf(N.x * N.x + N.z * N.z);
	else
		Nt = Vector3(0, -N.z, N.y) / sqrtf(N.y * N.y + N.z * N.z);
	Nb = N.cross(Nt);
}


Vector3 Raytracer::DirectIllumination(const Vector3& P, const Vector3& N, const Vector3& lightCentre, const float lightRadius, const Vector3& lightColour, const float cutoff)
{
	// calculate normalized light vector and distance to sphere light surface
	float r = lightRadius;
	Vector3 L = lightCentre - P;
	float distance = L.length();
	float d = max(distance - r, 0.0f);
	L /= distance;

	// calculate basic attenuation
	float denom = d / r + 1;
	float attenuation = 1 / (denom*denom);

	// scale and bias attenuation such that:
	//   attenuation == 0 at extent of max influence
	//   attenuation == 1 when d == 0
	attenuation = (attenuation - cutoff) / (1 - cutoff);
	attenuation = max(attenuation, 0.0f);

	float dot = max(L.dot(N), 0.0f);
	return lightColour * dot * attenuation;
}


Vector3 Raytracer::Trace(const Ray & ray, const vector<Object*>& objects, const int & depth) const
{
	// RUSSIAN ROULETTE
	float probability = 1.0f;
	bool result = RussianRoulette(depth, probability);
	if (!result)
		return Vector3(0, 0, 0);

	probability = 1.0f / probability;


	// PRIMARY RAY
	float fNear = INFINITY;
	
	Object* object = nullptr;
	for (unsigned int i = 0; i < objects.size(); ++i)
	{
		if (objects[i]->Intersects(ray, fNear))
		{
			object = objects[i];
		}
	}

	if (object != nullptr)
	{
		Vector3 hitPoint = ray.origin + fNear * ray.direction;
		Vector3 normal, color;
		object->GetNormalAndColor(hitPoint, normal, color);

		if (object->material == Standard_Material)
		{
			const float pdf = 1.0f / (2.0f * M_PI);

			Vector3 indirectDiffuse(0, 0, 0);
			{
				Vector3 Nt, Nb;
				createCoordinateSystem(normal, Nt, Nb);

				/*const uint32_t N = 1;
				const float invSample = 1.0f / static_cast<float>(N);

				for (uint32_t n = 0; n < N; ++n)*/
				{
					float r1 = distribution(generator);
					float r2 = distribution(generator);
					Vector3 sample = uniformSampleHemisphere(r1, r2);

					Vector3 sampleWorld(
						sample.x * Nb.x + sample.y * normal.x + sample.z * Nt.x,
						sample.x * Nb.y + sample.y * normal.y + sample.z * Nt.y,
						sample.x * Nb.z + sample.y * normal.z + sample.z * Nt.z);


					indirectDiffuse += (r1 * Trace(Ray(hitPoint + sampleWorld * 0.001f, sampleWorld), objects, depth + 1) / pdf); // *invSample;
				}
			}

			Vector3 directSpecular(0, 0, 0);
			Vector3 directDiffuse(0, 0, 0);
			/*{
				float fNear2 = INFINITY;
				
				const Vector3 pos = m_lightDirection * -100.0f;
				const float radius = 10.0f;

				Vector3 rand = Vector3(distribution(generator) - 0.5f,
					distribution(generator) - 0.5f,
					distribution(generator) - 0.5f).normalize() * radius;
				Vector3 direction = (pos + rand) - hitPoint;
				direction = direction.normalize();

				//Object* obj = nullptr;
				float intensity = 1.0f;
				for (unsigned int i = 0; i < objects.size(); ++i)
				{
					if (objects[i]->Intersects(Ray(hitPoint + direction * 0.001f, direction), fNear2))
					{
						intensity = 0.0f;
						break;
					}
				}

				if (intensity > 0.0f)
				{
					const Vector3 halfwayVector = (direction - ray.direction).normalize();
					const float specTmp = max(normal.dot(halfwayVector), 0.0);
					const float cShininess = 512.0f;
					const float specularIntensity = pow(specTmp, cShininess);


					const float NdotL = max(normal.dot(direction), 0.0f);
					directDiffuse = m_lightColor * NdotL;
					directSpecular = m_lightColor * specularIntensity;
				}
			}*/

			Vector3 diffuse = (directDiffuse + indirectDiffuse) * color / M_PI;


			Vector3 rayDir = ray.direction;
			Vector3 reflect = rayDir - normal * 2.0f * (rayDir.dot(normal));

			Vector3 reflection = Trace(Ray(hitPoint + reflect * 0.001f, reflect), objects, depth + 1);

			const float ior = 1.45f;
			float kr = 0.0f;
			fresnel(rayDir, normal, ior, kr);

			Vector3 finalColor = (diffuse * (1 - kr) * color) + (reflection * kr) + directSpecular;
			return finalColor * probability;
		}
		if (object->material == Chrome_Material)
		{
			Vector3 rayDir = ray.direction;
			Vector3 reflect = rayDir - normal * 2 * (rayDir.dot(normal));
			Vector3 reflection = Trace(Ray(hitPoint + reflect * 0.001f, reflect), objects, depth + 1);
			return reflection * color * probability;
		}
		if (object->material == Emissive_Material)
		{
			return color * probability;
		}
	}

	return backgroundColor;
}
