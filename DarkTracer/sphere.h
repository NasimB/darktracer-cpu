#pragma once

#include "object.h"

class Sphere : public Object
{
public:
	Sphere();
	Sphere(Vector3 position, float radius);
	Sphere(Vector3 position, float radius, Vector3 color);
	~Sphere();
	

	// Returns true if it intersects with the ray and is the closest to its origin, false otherwise.
	bool Intersects(const Ray& ray, float &near) override;
	void GetNormalAndColor(const Vector3& hitpoint, Vector3& normal, Vector3& color) override;

	float radius;

private:
	bool SolveQuadratic(const float& a, const float& b, const float& c, float& x0, float& x1) const;
};
